<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Storage;

class SendEmails extends Command
{
    protected $signature = 'mail:send {user}';

    protected $description = 'Send a marketing email to a user';

    public function handle(): int
    {
        Storage::disk('local')->put('command-test.txt', Date::now()->toJSON());

        return Command::SUCCESS;
    }
}
