<?php

namespace App\Console\Commands;

use App\Jobs\ProcessPodcast;
use Illuminate\Console\Command;

class TestJobs extends Command
{
    protected $signature = 'job:test';

    public function handle(): int
    {
        ProcessPodcast::dispatch();

        return Command::SUCCESS;
    }
}
